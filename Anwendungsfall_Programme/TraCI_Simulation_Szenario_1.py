#!/usr/bin/env python

import os
import sys
import optparse
import pandas as pd
import traci

# we need to import some python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # Checks for the binary in environ vars

def get_options():
    opt_parser = optparse.OptionParser()
    opt_parser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = opt_parser.parse_args()
    return options


# contains TraCI control loop
def run():
    step = 0
    while traci.simulation.getMinExpectedNumber() > 0:
        traci.simulationStep()

        if step == 0:  # 5 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 12.25)  # 44,1 km/h
        if step == 3600:  # 6 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 11.16)  # 40,2 km/h
        if step == 7200:  # 7 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.47)  # 37,7 km/h
        if step == 10800:  # 8 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.5)  # 38,0 km/h
        if step == 14400:  # 9 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.81)  # 38,9 km/h
        if step == 18000:  # 10 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.86)  # 39,1 km/h
        if step == 21600:  # 11 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.83)  # 39,0 km/h
        if step == 25200:  # 12 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.75)  # 38,7 km/h
        if step == 28800:  # 13 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.64)  # 38,3 km/h
        if step == 32400:  # 14 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.38)  # 37,4 km/h
        if step == 36000:  # 15 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.14)  # 36,5 km/h
        if step == 39600:  # 16 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.1)  # 36,4 km/h
        if step == 43200:  # 17 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 10.42)  # 37,5 km/h
        if step == 46800:  # 18 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 11.06)  # 39,8 km/h
        if step == 50400:  # 19 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 11.72)  # 42,2 km/h
        if step == 54000:  # 20 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 12.1)  # 43,6 km/h
        if step == 57600:  # 21 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 12.33)  # 44,4 km/h
        if step == 61200:  # 22 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 12.53)  # 45,1 km/h
        if step == 64800:  # 23 Uhr
            for i in traci.edge.getIDList():
                traci.edge.setMaxSpeed(i, 12.72)  # 45,8 km/h

        step += 1

    traci.close()
    sys.stdout.flush()


# main entry point
if __name__ == "__main__":
    options = get_options()

    # check binary
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    # traci starts sumo as a subprocess and then this script connects and runs
    traci.start([sumoBinary, "-c", r"Anwendungsfall_Szenario_1.sumocfg"])
    run()
