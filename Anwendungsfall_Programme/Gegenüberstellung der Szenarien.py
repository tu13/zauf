import pandas as pd

pd.options.display.float_format = "{:.2f}".format

df = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
df4 = pd.DataFrame()

abfrage = input('Wie viele Szenarien sollen miteinander verglichen werden? (bis zu 4 möglich) ')

columns_to_change = [
     'emissions_CO2_abs', 'emissions_CO_abs', 'emissions_HC_abs', 'emissions_NOx_abs', 'emissions_PMx_abs',
     'emissions_electricity_abs', 'emissions_fuel_abs']

if abfrage == '2':
    df = pd.read_excel(r"Gesamtergebnis_Szenario_1.xlsx")
    df2 = pd.read_excel(r"Gesamtergebnis_Szenario_2_GVZ.xlsx")
    df['tripinfo_routeLength'] = df['tripinfo_routeLength'] / 1000
    df2['tripinfo_routeLength'] = df2['tripinfo_routeLength'] / 1000
    fzgtyp = df.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp2 = df2.groupby('tripinfo_vType').count()['tripinfo_arrival']
    for i in columns_to_change:
        df[i] = df[i] / 1000000000000
        df2[i] = df2[i] / 1000000000000
        if i == 'emissions_fuel_abs':
            print('Szenario 1: ', i, ' ', round((df[i].sum()*1000), 2))
        else:
            print('Szenario 1: ', i, ' ', round(df[i].sum(),2))

        if i == 'emissions_fuel_abs':
            print('Szenario 2: ', i, ' ', round((df2[i].sum()*1000), 2))
        else:
            print('Szenario 2: ', i, ' ', round(df2[i].sum(),2))

    print('In Szenario 1 (Basis-Szenario) liegen die CO2-Emissionen bei ', round(df['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('In Szenario 2 liegen die CO2-Emissionen bei ', round(df2['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df2['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df2['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df2['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df2['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df2['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum()*1000) - (df2['emissions_fuel_abs'].sum()*1000), 2), 'l geringer.')
    print('Szenario 1 Flotte: ', fzgtyp)
    print('Szenario 2 Flotte: ', fzgtyp2)
    print(' ')

elif abfrage == '3':
    df = pd.read_excel(r"Gesamtergebnis_Szenario_1.xlsx")
    df2 = pd.read_excel(r"Gesamtergebnis_Szenario_2_GVZ.xlsx")
    df3 = pd.read_excel(r"Gesamtergebnis_Szenario_3_GVZ.xlsx")
    df['tripinfo_routeLength'] = df['tripinfo_routeLength'] / 1000
    df2['tripinfo_routeLength'] = df2['tripinfo_routeLength'] / 1000
    df3['tripinfo_routeLength'] = df3['tripinfo_routeLength'] / 1000
    fzgtyp = df.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp2 = df2.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp3 = df3.groupby('tripinfo_vType').count()['tripinfo_arrival']
    for i in columns_to_change:
        df[i] = df[i] / 1000000000000
        df2[i] = df2[i] / 1000000000000
        df3[i] = df3[i] / 1000000000000
        if i == 'emissions_fuel_abs':
            print('Szenario 1: ', i, ' ', round((df[i].sum()*1000), 2))
        else:
            print('Szenario 1: ', i, ' ', round(df[i].sum(),2))

        if i == 'emissions_fuel_abs':
            print('Szenario 2: ', i, ' ', round((df2[i].sum()*1000), 2))
        else:
            print('Szenario 2: ', i, ' ', round(df2[i].sum(),2))

        if i == 'emissions_fuel_abs':
            print('Szenario 3: ', i, ' ', round((df3[i].sum()*1000), 2))
        else:
            print('Szenario 3: ', i, ' ', round(df3[i].sum(),2))

    print('In Szenario 1 (Basis-Szenario) liegen die CO2-Emissionen bei ', round(df['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('In Szenario 2 liegen die CO2-Emissionen bei ', round(df2['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df2['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df2['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df2['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df2['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df2['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum()*1000) - (df2['emissions_fuel_abs'].sum()*1000), 2), 'l geringer.')
    print('In Szenario 3 liegen die CO2-Emissionen bei ', round(df3['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df3['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df3['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df3['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df3['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df3['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum()*1000) - (df3['emissions_fuel_abs'].sum()*1000), 2), 'l geringer.')
    print('Szenario 1 Flotte: ', fzgtyp)
    print('Szenario 2 Flotte: ', fzgtyp2)
    print('Szenario 3 Flotte: ', fzgtyp3)

elif abfrage == '4':
    df = pd.read_excel(r"Gesamtergebnis_Szenario_1.xlsx")
    df2 = pd.read_excel(r"Gesamtergebnis_Szenario_2_GVZ.xlsx")
    df3 = pd.read_excel(r"Gesamtergebnis_Szenario_3_GVZ.xlsx")
    df4 = pd.read_excel(r"Gesamtergebnis_Szenario_4_GVZ.xlsx")
    df['tripinfo_routeLength'] = df['tripinfo_routeLength'] / 1000
    df2['tripinfo_routeLength'] = df2['tripinfo_routeLength'] / 1000
    df3['tripinfo_routeLength'] = df3['tripinfo_routeLength'] / 1000
    df4['tripinfo_routeLength'] = df4['tripinfo_routeLength'] / 1000
    fzgtyp = df.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp2 = df2.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp3 = df3.groupby('tripinfo_vType').count()['tripinfo_arrival']
    fzgtyp4 = df4.groupby('tripinfo_vType').count()['tripinfo_arrival']
    for i in columns_to_change:
        df[i] = df[i] / 1000000000000
        df2[i] = df2[i] / 1000000000000
        df3[i] = df3[i] / 1000000000000
        df4[i] = df4[i] / 1000000000000
        if i == 'emissions_fuel_abs':
            print('Szenario 1: ', i, ' ', round((df[i].sum() * 1000), 2))
        else:
            print('Szenario 1: ', i, ' ', round(df[i].sum(), 2))

        if i == 'emissions_fuel_abs':
            print('Szenario 2: ', i, ' ', round((df2[i].sum() * 1000), 2))
        else:
            print('Szenario 2: ', i, ' ', round(df2[i].sum(), 2))

        if i == 'emissions_fuel_abs':
            print('Szenario 3: ', i, ' ', round((df3[i].sum() * 1000), 2))
        else:
            print('Szenario 3: ', i, ' ', round(df3[i].sum(), 2))

        if i == 'emissions_fuel_abs':
            print('Szenario 4: ', i, ' ', round((df4[i].sum() * 1000), 2))
        else:
            print('Szenario 4: ', i, ' ', round(df4[i].sum(), 2))

    print('In Szenario 1 (Basis-Szenario) liegen die CO2-Emissionen bei ', round(df['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('In Szenario 2 liegen die CO2-Emissionen bei ', round(df2['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df2['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df2['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df2['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df2['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df2['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum()*1000) - (df2['emissions_fuel_abs'].sum()*1000), 2), 'l geringer.')
    print('In Szenario 3 liegen die CO2-Emissionen bei ', round(df3['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df3['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df3['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df3['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df3['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df3['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum()*1000) - (df3['emissions_fuel_abs'].sum()*1000), 2), 'l geringer.')
    print('In Szenario 4 liegen die CO2-Emissionen bei ', round(df4['emissions_CO2_abs'].sum(), 2),
          ' kg und es wurden ', round(df4['tripinfo_routeLength'].sum(), 2), ' km gefahren. Dafür wurden ',round((df4['emissions_fuel_abs'].sum()*1000), 2),' l Kraftstoff verbraucht.')
    print('Die Einsparungen liegen hier also bei ',
          round(df['emissions_CO2_abs'].sum() - df4['emissions_CO2_abs'].sum(), 2), ' kg CO2 bzw. ',
          round((1 - (df4['emissions_CO2_abs'].sum() / df['emissions_CO2_abs'].sum())) * 100, 2), ' % und bei ',
          round(df['tripinfo_routeLength'].sum() - df4['tripinfo_routeLength'].sum(), 2), ' km. Der Kraftstoffverbrauch ist um ',
          round((df['emissions_fuel_abs'].sum() * 1000) - (df4['emissions_fuel_abs'].sum() * 1000), 2), 'l geringer.')
    print('Szenario 1 Flotte: ', fzgtyp)
    print('Szenario 2 Flotte: ', fzgtyp2)
    print('Szenario 3 Flotte: ', fzgtyp3)
    print('Szenario 4 Flotte: ', fzgtyp4)