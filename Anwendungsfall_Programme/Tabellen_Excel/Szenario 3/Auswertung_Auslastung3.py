import pandas as pd

pd.options.display.float_format = "{:.2f}".format

df = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
df4 = pd.DataFrame()

df = pd.read_excel(r"Fahrtenbuch_GVZ_gebuendelt_3_Stunden.xlsx")

auslastung_18t = round((df[df['Fahrzeugart'] == 'lkw_18']['Gewicht_gesamt'].mean()/9)* 100,2)
auslastung_7_5t = round((df[df['Fahrzeugart'] == 'lkw_7_5']['Gewicht_gesamt'].mean()/4)* 100,2)
auslastung_3_5t = round((df[df['Fahrzeugart'] == 'lkw_3_5']['Gewicht_gesamt'].mean()/1.2)* 100,2)

print('Auslastung 18t in %: ',auslastung_18t)
print('Auslastung 7,5t in %: ',auslastung_7_5t)
print('Auslastung 3,5t in %: ',auslastung_3_5t)
