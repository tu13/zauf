# Programm generiert die XML-Dateien für SUMO
# Quelle für die Fahrten vom Rand aus: "Fahrtenbuch_Rand_sortiert_nach_Abfahrtszeit.xlsx"
# quelle für die gebündelten Fahrten vom GVZ aus: "Fahrtenbuch_GVZ_gebuendelt.xlsx"
# Output: "Touren_Rand.trips.xml" und "Touren_GVZ.trips.xml"

import xml.etree.cElementTree as ET
import pandas as pd

def indent(elem, level=0): # erzeugt Zeilenumbruch
  i = "\n" + level*"  "
  if len(elem):
    if not elem.text or not elem.text.strip():
      elem.text = i + "  "
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
    for elem in elem:
      indent(elem, level+1)
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
  else:
    if level and (not elem.tail or not elem.tail.strip()):
      elem.tail = i

## 1: nicht gebündelte Aufträge, Start von Rand
# Einlesen der Excel-Datei
df = pd.read_excel(r"Tabellen_Excel\Fahrtenbuch_Rand_sortiert_nach_Abfahrtszeit.xlsx")

# Einlesen der Excel-Spalten
Trip_ID = df['Auftrag_ID'].tolist()
Abfahrt = df['Abfahrt [s]'].tolist()
Start = df['Start'].tolist()
Ziel = df['Ziel'].tolist()
Fahrzeugart = df['Fahrzeugart'].tolist()

vclass1 = '\"truck\" '
vclass2 = '\"truck\" '
vclass3 = '\"delivery\" '

Anzahl_Zeilen = len(df.index)

# Beginn Trips-Datei schreiben
root = ET.Element("routes")
# Deklasrieren der Fahrzeugarten
vtypelist = ['vType id="lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="red" guiShape="truck/trailer" length="10" emissionClass="HBEFA2/HDV_12_12"', 'vType id="lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="blue" emissionClass="HBEFA2/HDV_12_2"', 'vType id="lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="yellow" emissionClass="HBEFA2/P_14_5"', 'vType id="E_lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" guiShape="truck/trailer" length="10" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"']
for i in vtypelist:
    x = ET.Element(i)
    root.append(x)

# erzeugen der Trips
j = 0
while j < Anzahl_Zeilen:
    trip = ET.SubElement(root, "trip", {"id": str(Trip_ID[j]), "type": str(Fahrzeugart[j]), "depart": str(Abfahrt[j]), "fromJunction": str(Start[j]), "toJunction": str(Ziel[j]), "departPos":"base", "departLane":"best"})
    Stop = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_" + str(Ziel[j]), "duration": "600"})
    j += 1

indent(root)

tree = ET.ElementTree(root)
tree.write('XML_Dateien\Touren_Rand.trips.xml')


## 2: gebündelte Aufträge, Start von GVZ
# Einlesen der Excel-Datei
df = pd.read_excel(r"Tabellen_Excel\Fahrtenbuch_GVZ_gebuendelt_3_Stunden.xlsx")

# Einlesen der Excel-Spalten
Trip_ID = df['Auftrags_ID'].tolist()
Abfahrt = df['Abfahrt'].tolist()
Start = df['Start'].tolist()
Stopp_1 = df['Stopp_1'].tolist()
Stopp_2 = df['Stopp_2'].tolist()
Stopp_3 = df['Stopp_3'].tolist()
Ziel = df['Ziel'].tolist()
Fahrzeugart = df['Fahrzeugart'].tolist()

vclass1 = '\"truck\" '
vclass2 = '\"truck\" '
vclass3 = '\"delivery\" '

Anzahl_Zeilen = len(df.index)

# Beginn Trips-Datei schreiben
root = ET.Element("routes")
# Deklasrieren der Fahrzeugarten
vtypelist = ['vType id="lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="red" guiShape="truck/trailer" length="10" emissionClass="HBEFA2/HDV_12_12"', 'vType id="lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="blue" emissionClass="HBEFA2/HDV_12_2"', 'vType id="lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="yellow" emissionClass="HBEFA2/P_14_5"', 'vType id="E_lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" guiShape="truck/trailer" length="10" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"']
for i in vtypelist:
    x = ET.Element(i)
    root.append(x)

# erzeugen der Trips
j = 0
while j < Anzahl_Zeilen:
    trip = ET.SubElement(root, "trip", {"id": str(Trip_ID[j]), "type": str(Fahrzeugart[j]), "depart": str(Abfahrt[j]),
                                        "fromJunction": str(Start[j]), "toJunction": str(Ziel[j]),
                                        "departPos": "base","departLane": "best"})
    if pd.Series([Stopp_1[j]]).isnull().values.any() == False:
        Stop1 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_1[j], "duration": "600"})
    if pd.Series([Stopp_2[j]]).isnull().values.any() == False:
        if Stopp_2[j] == Stopp_1[j]:
            Dauer = 200
        else:
            Dauer = 600
        Stop2 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_2[j], "duration": str(Dauer)})
    if pd.Series([Stopp_3[j]]).isnull().values.any() == False:
        if Stopp_3[j] == Stopp_2[j]:
            Dauer = 200
        else:
            Dauer = 600
        Stop3 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_3[j], "duration": str(Dauer)})
    if Ziel[j] == Stopp_1[j] or Ziel[j] == Stopp_2[j] or Ziel[j] == Stopp_3[j]:
        Dauer = 200
    else:
        Dauer = 600
    Ziel2 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_" + Ziel[j], "duration": str(Dauer)})
    j+=1

indent(root)

tree = ET.ElementTree(root)
tree.write('XML_Dateien\Touren_GVZ_3_Stunden.trips.xml')


## 3: gebündelte Aufträge, aber mehr kleinere Fahrzeuge, Start von GVZ
# Einlesen der Excel-Datei
#df = pd.read_excel(r"Tabellen_Excel\Fahrtenbuch_GVZ_gebuendelt_5_Stunden.xlsx")

# Einlesen der Excel-Spalten
#Trip_ID = df['Auftrags_ID'].tolist()
#Abfahrt = df['Abfahrt'].tolist()
#Start = df['Start'].tolist()
#Stopp_1 = df['Stopp_1'].tolist()
#Stopp_2 = df['Stopp_2'].tolist()
#Stopp_3 = df['Stopp_3'].tolist()
#Ziel = df['Ziel'].tolist()
#Fahrzeugart = df['Fahrzeugart'].tolist()

#vclass1 = '\"truck\" '
#vclass2 = '\"truck\" '
#vclass3 = '\"delivery\" '

#Anzahl_Zeilen = len(df.index)

# Beginn Trips-Datei schreiben
#root = ET.Element("routes")
# Deklasrieren der Fahrzeugarten
#vtypelist = ['vType id="lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="red" guiShape="truck/trailer" length="10" emissionClass="HBEFA2/HDV_12_12"', 'vType id="lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="blue" emissionClass="HBEFA2/HDV_12_2"', 'vType id="lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="yellow" emissionClass="HBEFA2/P_14_5"', 'vType id="E_lkw_18" vClass=' + vclass1 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" guiShape="truck/trailer" length="10" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_7_5" vClass=' + vclass2 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"', 'vType id="E_lkw_3_5" vClass=' + vclass3 + 'maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="green" emissionClass="Energy/unknown"']
#for i in vtypelist:
#    x = ET.Element(i)
#    root.append(x)

# erzeugen der Trips
#j = 0
#while j < Anzahl_Zeilen:
#    trip = ET.SubElement(root, "trip", {"id": str(Trip_ID[j]), "type": str(Fahrzeugart[j]), "depart": str(Abfahrt[j]),
#                                        "fromJunction": str(Start[j]), "toJunction": str(Ziel[j]),
#                                        "departPos": "base","departLane": "best"})
#    if pd.Series([Stopp_1[j]]).isnull().values.any() == False:
#        Stop1 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_1[j], "duration": "600"})
#    if pd.Series([Stopp_2[j]]).isnull().values.any() == False:
#        if Stopp_2[j] == Stopp_1[j]:
#            Dauer = 200
#        else:
#            Dauer = 600
#        Stop2 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_2[j], "duration": str(Dauer)})
#    if pd.Series([Stopp_3[j]]).isnull().values.any() == False:
#        if Stopp_3[j] == Stopp_2[j]:
#            Dauer = 200
#        else:
#            Dauer = 600
#        Stop3 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_"+Stopp_3[j], "duration": str(Dauer)})
#    if Ziel[j] == Stopp_1[j] or Ziel[j] == Stopp_2[j] or Ziel[j] == Stopp_3[j]:
#        Dauer = 200
#    else:
#        Dauer = 600
#    Ziel2 = ET.SubElement(trip, "stop", {"parkingArea": "parkingArea_" + Ziel[j], "duration": str(Dauer)})
#    j+=1

#indent(root)

#tree = ET.ElementTree(root)
#tree.write('XML_Dateien\Touren_GVZ_5_Stunden.trips.xml')