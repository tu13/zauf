# im Programm wird die XML-Datei für die Vorstudie (Bestimmen Fahrzeiten zwischen Start- und Zielpunkten) erstellt
# Ergebnis ist Input für Konfigurationsdatei "Vorstudie.sumocfg"
import xml.etree.cElementTree as ET
import pandas as pd


def indent(elem, level=0):  # erzeugt Zeilenumbruch
  i = "\n" + level*"  "
  if len(elem):
    if not elem.text or not elem.text.strip():
      elem.text = i + "  "
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
    for elem in elem:
      indent(elem, level+1)
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
  else:
    if level and (not elem.tail or not elem.tail.strip()):
      elem.tail = i


Anzahl_Fabriken = 8
Anzahl_Start = 8


# Beginn Trips-Datei schreiben
root = ET.Element("routes")
# Deklarieren der Fahrzeugarten
vtypelist = ['vType id="lkw_7_5" vClass= "truck" maxSpeed="40" speedFactor="1" speedDev="0" sigma="0.5" color="yellow"']
for i in vtypelist:
    x = ET.Element(i)
    root.append(x)
Abfahrt = 0
# erzeugen der Trips
# GVZ zu Fabriken
for i in range(Anzahl_Fabriken):
    trip = ET.SubElement(root, "trip",
                         {"id": "GVZ_to_Fabrik_" + str(i + 1), "type": "lkw_7_5", "depart": str(Abfahrt),
                          "fromJunction": "GVZ", "toJunction": "Fabrik_" + str(i + 1),
                          "departPos": "base",
                          "departLane": "best"})
    Abfahrt = Abfahrt + 10
for j in range(Anzahl_Start):  # von jedem Start zu jeder Fabrik
    for i in range(Anzahl_Fabriken):
        trip = ET.SubElement(root, "trip",
                             {"id": "Start_"+ str(j+1)+"_to_Fabrik_"+str(i+1), "type": "lkw_7_5", "depart": str(Abfahrt),
                              "fromJunction": "Start_" + str(j+1), "toJunction": "Fabrik_" + str(i+1),
                              "departPos": "base", "departLane": "best"})
        Abfahrt = Abfahrt + 10
for j in range(Anzahl_Fabriken):  # Fabriken untereinander
    for i in range(Anzahl_Fabriken):
        if j != i:
            trip = ET.SubElement(root, "trip",
                             {"id": "Fabrik_"+ str(j+1)+"_to_Fabrik_"+str(i+1), "type": "lkw_7_5", "depart": str(Abfahrt),
                              "fromJunction": "Fabrik_" + str(j+1), "toJunction": "Fabrik_" + str(i+1),
                              "departPos": "base", "departLane": "best"})
            Abfahrt = Abfahrt + 10

indent(root)

tree = ET.ElementTree(root)
tree.write('XML_Dateien\Vorstudie.trips.xml')