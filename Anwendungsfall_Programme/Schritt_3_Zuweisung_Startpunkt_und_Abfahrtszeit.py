# im Programm werden dem Fahrtenbuch Abfahrtszeiten und Startpunkte zugewiesen
# vom Anwender wird die Angabe, ob ein GVZ verwendet werden soll, benötigt (ja oder nein)
# ggf. muss das Programm 2 mal ausgeführt werden (einmal für GVZ, einmal ohne GVZ)

import random
import pandas as pd
import numpy as np

# Einlesen der Excel-Dateien als DataFrame df und fahrzeit
df = pd.read_excel(r"Tabellen_Excel\Auftragsbuch_sortiert_nach_Fabrik.xlsx")
fahrzeit = pd.read_excel(r"Tabellen_Excel\Entfernungsmatrix_Fahrzeit.xlsx", index_col="Index")

# weitere Spalten hinzufügen
df.insert(2, "Start", np.nan)
df.insert(4, "Abfahrt [s]", np.nan)
df.rename(columns={'Ankunft [s]': 'Ankunft (geplant) [s]'}, inplace=True)

gvzabfrage = input('Soll ein GVZ berücksichtigt werden? ')  # Abfrage GVZ oder nicht, Antwort: ja oder nein
anzahl_starts = 8  # wenn nicht GVZ
Gesamtanzahl_Auftraege = len(df.index)

# Funktion zum Startpunkt setzen (braucht keinen Eingabewert)
def bestimme_Start():
    if gvzabfrage == 'ja':
        Start_i = 'GVZ'
    else:
        startpunkte = list()
        for s in range(anzahl_starts):
            startpunkte.append('Start_' + str(s + 1))  # [Start_1,Start_2,...]
        Start_i = random.choice(startpunkte)
    return Start_i


# Funktion berechnet Abfahrtszeit zum Auftrag i
def ab(i):
    Abfahrt_i = df.at[i, 'Ankunft (geplant) [s]'] - fahrzeit.at[df.at[i, 'Start'],df.at[i, 'Ziel']]
    Abfahrt_i = round(Abfahrt_i, 0)  # keine Nachkommastelle
    return Abfahrt_i


i = 0
while i < Gesamtanzahl_Auftraege:  # i ist Index im Dataframe
    # --> i muss also ganze Zahl ab 0 sein, Fehler, falls Index umgenannt wurde
    Startpunkt = bestimme_Start()
    df.loc[i,'Start'] = [str(Startpunkt)]  # Schreiben in DataFrame
    Abfahrt = ab(i)
    df.loc[i, 'Abfahrt [s]'] = [Abfahrt]  # Schreiben in DataFrame
    i += 1

# Wieder Index richtig setzen, vorheriger Index war eigene Spalte
df.set_index('Unnamed: 0', inplace=True)  # Spalte Unnamed: 0 wird als Index verwendet
df.index.name = None  # löscht die leere Zeile oben im DataFrame

# nach Abfahrt sortieren
df_sorted = df.sort_values(by=['Abfahrt [s]'])

# als Excel speichern
if gvzabfrage == 'ja':
    with pd.ExcelWriter('Tabellen_Excel\Fahrtenbuch_GVZ_sortiert_nach_Abfahrtszeit.xlsx') as writer:
        df_sorted.to_excel(writer)
    with pd.ExcelWriter('Tabellen_Excel\Fahrtenbuch_GVZ_sortiert_nach_Fabrik.xlsx') as writer:
        df.to_excel(writer)
else:
    with pd.ExcelWriter('Tabellen_Excel\Fahrtenbuch_Rand_sortiert_nach_Abfahrtszeit.xlsx') as writer:
        df_sorted.to_excel(writer)
    with pd.ExcelWriter('Tabellen_Excel\Fahrtenbuch_Rand_sortiert_nach_Fabrik.xlsx') as writer:
        df.to_excel(writer)
