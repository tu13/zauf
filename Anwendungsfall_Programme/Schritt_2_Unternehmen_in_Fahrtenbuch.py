# Programm generiert auf Basis der Excel-Datei "Definition_Unternehmen.xlsx" das Auftrags- bzw. Fahrtenbuch
# als Excel-Datei "Auftragsbuch_*.xlsx"

import random
import pandas as pd
import numpy as np

# Einlesen der Excel-Datei als DataFrame df
df = pd.read_excel(r"C:\Users\Reineke\Desktop\Test\Anwendungsfall_Programme\Tabellen_Excel\Definition_Unternehmen.xlsx")

# Einlesen der Excel-Spalten in den DataFrame df
Fabrik_Name = df['Name des Unternehmens'].tolist()
Anzahl_Auftraege = df['Gesamtanzahl Belieferungen im Simulationszeitraum'].tolist()
Anteil_Nacht = df['Anteil Nachtfahrten zwischen 22 - 06 Uhr [%]'].tolist()
Anteil_Tag = df['Anteil Tagfahrten zwischen 06 - 22 Uhr [%]'].tolist()
JIT_JIS_Anteil = df['Anteil JIT / JIS der Fahrten [%]'].tolist()
JIT_JIS_Zeitfenster = df['Toleranz-zeitfenster JIT / JIS (+/-) [s]'].tolist()
Gewicht_min = df['minimales Gewicht der Ladung [t]'].tolist()
Gewicht_max = df['maximales Gewicht der Ladung [t]'].tolist()
Produktgruppe = df['Produkt-gruppe'].tolist()

Simulationszeit = 61200  # Beginn bei 0 = 5 Uhr morgens, Ende bei 61200 = 22 Uhr abends, Fahrzeuge ab 6 Uhr, d.h. 3600

# Fabrik_Nummer wird später für Zugriff Listenelement benötigt
Fabrik_Nummern = list(range(1, len(Fabrik_Name)+1))


# Bestimmen der Ankunftszeiten, Eingabe i ist Fabriknummer
def ankunftszeiten(i):  # schreibt Ankunftszeiten in Liste
    j = 0
    ankunft = 3600
    b = list()  # leere Liste
    while j < Anzahl_Auftraege[i - 1]:  # i-1, da Fabrik_Nummern bei 1 beginnt, Liste aber bei Index 0
        Abstand_min = Simulationszeit/Anzahl_Auftraege[i-1]-Simulationszeit/(Anzahl_Auftraege[i-1]*2)
        Abstand_max = Simulationszeit/Anzahl_Auftraege[i-1]+Simulationszeit/(Anzahl_Auftraege[i-1]*2)
        ankunft = ankunft + random.randrange(int(Abstand_min), int(Abstand_max))
        b.append(ankunft)
        j += 1
    return b


# Funktion bestimmt zufälliges Gewicht aus der Range, Eingabe i ist Fabriknummer
def gewicht(i):
    zufaelliges_Gewicht = round(random.uniform(Gewicht_min[i - 1], Gewicht_max[i - 1]), 1)  # eine Nachkommastelle
    return zufaelliges_Gewicht


# Zuordnung von Fahrzeug zu Gewicht, Eingabe ist (zufälliges) Gewicht
def Fahrzeug(i):
    if i <= 1.2:
        a = 'lkw_3_5'
    elif i <= 4:
        a = 'lkw_7_5'
    else:
        a = 'lkw_18'
    return a


# JIS-Anteil setzen, Eingabewert ist Fabriknummer
def Generate_JIT_JIS(i):
    j = 0
    b = list()
    while j < Anzahl:  # Anzahl ist Variable im Hauptprogramm
        if JIT_JIS_Anteil[i-1] > 0:
            Anteil = [JIT_JIS_Anteil[i-1], 100 - JIT_JIS_Anteil[i-1]]
            c = random.choices(['x', np.nan], weights=Anteil, k=1)
            if c == ['x']:
                a = 'x'
            else:
                a = np.nan
            b.append(a)
            j += 1
        else:
            b.append(np.nan)
            j += 1
    return b


# definieren DataFrame (der die einzelnen Aufträge enthält)
trips = pd.DataFrame({'Auftrag_ID': [],
                      'Ziel': [],
                      'Ankunft [s]': [],
                      'Gewicht der Ladung [t]': [],
                      'Fahrzeugart': [],
                      'JIT/JIS': [],
                      'Produktgruppe': []},
                     columns=['Auftrag_ID', 'Ziel', 'Ankunft [s]', 'Gewicht der Ladung [t]', 'Fahrzeugart', 'JIT/JIS',
                                'Produktgruppe'])

# DataFrame schreiben
for t in Fabrik_Nummern:  # geht alle Fabriken nacheinander durch
    Anzahl = Anzahl_Auftraege[t-1]  # Anzahl Aufträge von Fabrik t
    i = 0  # Laufvariable für die Anzahl Aufträge
    Ankunft_Fabrik = ankunftszeiten(t)
    while Ankunft_Fabrik[Anzahl-1] > Simulationszeit:  # da manchmal letzte Werte größer als Simulationszeit
        Ankunft_Fabrik = ankunftszeiten(t)  # Liste für alle Aufträge von Fabrik t
    JIT_JIS = Generate_JIT_JIS(t)
    while i < Anzahl:  # bestimmt die einzelnen Aufträge
        an = Ankunft_Fabrik[i]
        Gewicht = gewicht(t)
        type = Fahrzeug(Gewicht)
        trips.loc[str(t) + '_' + str(i), :] = ['Auftrag_' + str(i) + '_to_' + str(Fabrik_Name[t-1]),
                                               str(Fabrik_Name[t-1]), an, str(Gewicht), str(type), JIT_JIS[i],
                                               str(Produktgruppe[t-1])]
        i += 1
    # Anzahl = 0

# nach Abfahrt sortieren
trips_sorted = trips.sort_values(by=['Ankunft [s]'])

# als Excel speichern
with pd.ExcelWriter('Tabellen_Excel\Auftragsbuch_sortiert_nach_Ankunftszeit.xlsx') as writer:
    trips_sorted.to_excel(writer)
with pd.ExcelWriter('Tabellen_Excel\Auftragsbuch_sortiert_nach_Fabrik.xlsx') as writer:
    trips.to_excel(writer)
