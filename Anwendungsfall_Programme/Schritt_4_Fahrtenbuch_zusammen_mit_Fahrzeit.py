# im Programm werden die Fahrten gebündelt
# im ersten Teil werden die JIT- bzw. JIS-Aufträge zusammengefasst, danach die restlichen
# Bündelung erfolgt nach gleichem Ziel und maximalem Gewicht
# ggf. muss die Zeit im GVZ angepasst werden (Differenz_Abfahrten)
# Output: 'Tabellen_Excel\Fahrtenbuch_GVZ_gebuendelt.xlsx'

import pandas as pd
import numpy as np

# Einlesen des Fahrtenbuchs
df = pd.read_excel(r"Tabellen_Excel\Fahrtenbuch_GVZ_sortiert_nach_Abfahrtszeit.xlsx",
                   names=["ID", "Auftrags_ID", "Start", "Ziel", "Abfahrt", "Ankunft", "Gewicht_gesamt",
                          "Fahrzeugart", "JIT/JIS", "Produkt"])

# DataFrame sortieren, neue Spalten einfügen
df.drop('ID', axis=1)  # axis=1 heißt Spalte
del df['ID']  # löschen der Spalte 'ID'

Anzahl_Auftraege = len(df.index)
df.insert(2, "Stopp_1", [np.nan] * Anzahl_Auftraege, True)
df.insert(3, "Stopp_2", [np.nan] * Anzahl_Auftraege, True)
df.insert(4, "Stopp_3", [np.nan] * Anzahl_Auftraege, True)
df.insert(8, "Gewicht Stopp_1", [np.nan] * Anzahl_Auftraege, True)
df.insert(9, "Gewicht Stopp_2", [np.nan] * Anzahl_Auftraege, True)
df.insert(10, "Gewicht Stopp_3", [np.nan] * Anzahl_Auftraege, True)
df.insert(11, "Gewicht Ziel", [np.nan] * Anzahl_Auftraege, True)
df.insert(15, "nicht bündeln", [np.nan] * Anzahl_Auftraege, True)
df["Gewicht Ziel"] = df["Gewicht_gesamt"]

# Einlesen der Entfernungs-Matrix --> für die Berechnung der besten Strecke
fahrzeit = pd.read_excel(r"Tabellen_Excel\Entfernungsmatrix_Fahrzeit.xlsx", index_col="Index")

# Bündelungs-Restriktionen: Gewicht, Zeit, Abstand gesamt
Gewicht_max = 9  # maximales Gewicht, Nutzlast von 18-Tonner
Differenz_Abfahrten = 10800  # max. 3h zwischen zwei Aufträgen
Abstand_Fabriken_max = 10000  # maximale Tourenlänge in s zwischen Stopps, ohne Weg vom GVZ zum 1. Stopp
Fahrzeit_max = 5000  # maximale Fahrzeit in s einer Tour


# Definition der Funktionen
# Funktion prüft, ob es bereits Stopp_1 gibt
def pruefe_Stopp_1(j):  # Eingabe j ist Zeilennummer
    is_via = pd.Series([df.at[j, 'Stopp_1']])  # da Werte NaN enthalten sein können --> dieser Weg für Umgang mit NaN
    if is_via.isnull().values.any() == True:  # Wert ist Null
        return False  # False heißt kein Stopp_1 bisher
    else:
        return True  # es gibt schon Stopp_1


# Funktion zum Prüfen, ob Auftrag j JIT/JIS ist
def pruefe_JIT(j, a):  # Eingabe j ist betrachteter Auftrag, a ist Dataframe, von dem JIT bestimmt werden soll
    if a.at[j, 'JIT/JIS'] == 'x':
        return True  # Auftrag ist JIT/JIS
    else:
        return False


# Funktion um Vergleichszeilen in eigenen Dataframe zu schreiben, abhängig von Zeitintervall [grenze_unten,grenze_oben]
def erzeuge_vergleichs_df(j, grenze_unten, grenze_oben):  # j ist betrachter Auftrag
    a = df.copy()  # Kopie von df
    a['Index in df'] = a.index  # neue Spalte, wichtig damit später richtiger Auftrag aus df gelöscht wird
    t = 0
    while t in range(len(df.index)):
        if t == j:
            a = a.drop([t], axis=0)
            t += 1
        else:
            if a.at[t, 'Abfahrt'] < df.at[j, 'Abfahrt'] - grenze_unten:  # Werte außerhalb Zeitintervall löschen
                a = a.drop([t])
                t += 1
            elif a.at[t, 'Abfahrt'] > df.at[j, 'Abfahrt'] + grenze_oben:  # Werte außerhalb Zeitintervall löschen
                a = a.drop([t])
                t += 1
            else:
                t += 1
    a = a.reset_index(drop=True)  # Index zurück, falls es später vergessen wird
    return a


# Funktion um zu hohes Gesamtgewicht aus Vergleichsdataframe auszusortieren:
def aussortieren_Gewicht(j, a):  # j ist betrachtete Zeile (df), a ist DataFrame mit Vergleichszeilen
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife nicht funktioniert
    t = 0
    Anzahl_Vergleichs_Zeilen = len(a.index)
    a.insert(0, "Gewicht_gesamt_neu", [np.nan] * Anzahl_Vergleichs_Zeilen)  # neue Spalte zum Speichern Gesamtgewicht
    while t in range(Anzahl_Vergleichs_Zeilen):
        Gewicht = df.at[j, 'Gewicht_gesamt'] + a.at[t, 'Gewicht_gesamt']  # Gesamtgewicht berechnen
        if df.at[j, 'Gewicht_gesamt'] + a.at[t, 'Gewicht_gesamt'] > Gewicht_max:  # Gewicht zu groß --> löschen
            a = a.drop([t])
            t += 1
        else:
            a.at[t, 'Gewicht_gesamt_neu'] = Gewicht  # Gewicht in Spalte im Vergleichsdataframe speichern
            t += 1
    return a


# Funktion prüft, ob bereits zu viele Stopps enthalten: hier Anzahl Stopps auf 4 festgesetzt
def aussortieren_zu_viele_Stopps(j, a):  # j ist betrachtete Zeile, a ist Vergleichsdataframe
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife ggf. nicht funktioniert
    t = 0
    Anzahl_Vergleichs_Zeilen = len(a.index)  # Wert über den Schleife läuft
    while t in range(Anzahl_Vergleichs_Zeilen):
        # insgesamt 4 Stopps je Auftrag möglich, jeder hat 3 Stopps und 1 Ziel,
        # Ziel ist immer gesetzt, die anderen variieren, daher muss Ziel nicht betrachtet werden
        s_1_bei_j = pd.Series([df.at[j, 'Stopp_1']])  # Stopp_1 bei Auftrag j
        s_2_bei_j = pd.Series([df.at[j, 'Stopp_2']])
        s_3_bei_j = pd.Series([df.at[j, 'Stopp_3']])
        s_1_bei_t = pd.Series([a.at[t, 'Stopp_1']])
        s_2_bei_t = pd.Series([a.at[t, 'Stopp_2']])
        s_3_bei_t = pd.Series([a.at[t, 'Stopp_3']])

        if s_3_bei_j.isnull().values.any() == False or s_3_bei_t.isnull().values.any() == False:  # = bereits Stopp 3
            k = False  # false heißt Kombination allgemein nicht möglich
        elif s_2_bei_j.isnull().values.any() == False and s_2_bei_t.isnull().values.any() == False:  # = bereits Stopp 2
            k = False
        elif s_2_bei_j.isnull().values.any() == False and s_1_bei_t.isnull().values.any() == False:
            k = False
        elif s_1_bei_j.isnull().values.any() == False and s_2_bei_t.isnull().values.any() == False:
            k = False
        else:
            k = True  # true heißt Platz für weitere Stopps generell frei
        # Zeilen mit zuvielen Stopps aus VergleichsdataFrame löschen
        if k is False:
            a = a.drop([t])  # Zeile aus Vergleichsdataframe löschen
        t += 1
    return a


# Funktion sortiert Aufträge mit hoher Auslastung aus
def aussortieren_nicht_buendeln(a):  # a ist Vergleichsdataframe
    a = a.reset_index(drop=True)
    t = 0
    while t in range(len(a.index)):
        if a.at[t, "nicht bündeln"] == 1:
            a = a.drop([t], axis=0)
            t += 1
        else:
            t += 1
    a = a.reset_index(drop=True)
    return a


# Funktion minimaler Abstand zwischen den Stopps (noch ohne Rückweg)
def abstand_min(j, a):  # j ist Auftrag (df), t sind Zeilen des Vergleichdataframes, a ist Vergleichsdataframe
    a = a.reset_index(drop=True)
    Anzahl_Vergleichs_Zeilen = len(a.index)
    a.insert(0, "Fahrzeit", [np.nan] * len(a.index))  # erzeugen neue Spalte
    t = 0
    while t in range(Anzahl_Vergleichs_Zeilen):
        Fahrzeit_Fabriken = 0
        Liste_Stopps = [df.at[j, 'Ziel'], df.at[j, 'Stopp_1'], df.at[j, 'Stopp_2'], a.at[t, 'Ziel'], a.at[t, 'Stopp_1'],
                        a.at[t, 'Stopp_2']]
        Reihenfolge = []
        Start = 'GVZ'  # Wert über den Abstand berechnet wird, Start variiert
        h = 0
        while h >= 0:  # Schleife hat keine sonstige Funktion, aber damit Abbruchkriterium funktioniert
            Liste_Abstaende = []  # Abstand zu allen Stopps zum Startpunkt in Liste gespeichert
            if pd.Series(Liste_Stopps).isnull().values.all() == True:  # alle Ziele zugeordnet --> Schleife abbrechen
                break
            for m in range(6):  # Ergebnis ist Liste mit den Abständen
                is_nan = pd.Series([Liste_Stopps[m]])  # --> nötig, damit kein Fehler wegen nan entsteht
                if is_nan.isnull().values.any() == True:  # Eintrag nan ist --> der Liste wird auch nan hinzugefügt
                    Liste_Abstaende.append(np.nan)
                else:
                    Liste_Abstaende.append(fahrzeit.at[Start, Liste_Stopps[m]])  # Abstand zum Startpunkt berechnet
            Stelle_Min = Liste_Abstaende.index(np.nanmin(Liste_Abstaende))  # Position, an der minimaler Abstand ist
            Fahrzeit_Fabriken = Fahrzeit_Fabriken + np.nanmin(Liste_Abstaende)  # Minimum zum Gesamtabstand addieren
            Reihenfolge.append(Liste_Stopps[Stelle_Min])
            Start = Liste_Stopps[Stelle_Min]  # neuen Startpunkt setzen
            Liste_Stopps[Stelle_Min] = np.nan  # bereits dazu addiert -> d.h. zu nan setzen, sonst erneut betrachtet

        Fahrzeit_Fabriken_ab_Stopp_1 = Fahrzeit_Fabriken - fahrzeit.at['GVZ', Reihenfolge[0]]
        if Fahrzeit_Fabriken_ab_Stopp_1 > Fahrzeit_max:  # Restriktion maximale Fahrtenlänge
            a = a.drop([t])
        else:
            a.at[t, 'Fahrzeit'] = Fahrzeit_Fabriken_ab_Stopp_1
        t += 1
    return a


# Funktion zum prüfen, ob eine Kombination unter 3,5 t insgesamt (1,2 t Nutzlast) liegt
def ist_unter_3_5(a):  # a ist VergleichsdataFrame
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife nicht funktioniert
    Gewicht = a['Gewicht_gesamt_neu'].tolist()
    l = []
    for h in range(len(Gewicht)):
        if 1.1 <= Gewicht[h] <= 1.2:  # es gibt eine Kombination, mit der kleines Auto erreicht wird, 1,2 t Nutzlast für insgesamt 3,5 t
            l.append(h)
    if len(l) == 0:
        return False  # es gibt keine Kombinationsmöglichkeit unter 3,5 t Nutzlast
    else:
        return True  # es gibt Kombinationsmöglichkeiten unter 3,5 t Nutzlast


# Funktion um Fahrten auszusortieren, die nicht nah an 3,5 sind --> wenn ist_unter_3_5 True ist
def aussortieren_ueber_3_5(j, a):  # j ist betrachtete Zeile, a ist DataFrame mit Vergleichszeilen
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife nicht funktioniert
    t = 0
    Anzahl_Vergleichs_Zeilen = len(a.index)
    while t in range(Anzahl_Vergleichs_Zeilen):
        Gewicht = df.at[j, 'Gewicht_gesamt'] + a.at[t, 'Gewicht_gesamt']
        if Gewicht <= 1.1 or Gewicht >= 1.2:
            a = a.drop([t])
            t += 1
        else:
            t += 1
    return a


# Funktion zum prüfen, ob eine Kombination zwischen 6.5 und 7.5 liegt, sodass kleines Auto genutzt werden kann
def ist_unter_7_5(a):  # a ist VergleichsdataFrame
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife nicht funktioniert
    Gewicht = a['Gewicht_gesamt_neu'].tolist()
    l = []
    for h in range(len(Gewicht)):
        if Gewicht[h] >= 3.5 and Gewicht[h] <= 4:  # es gibt eine Kombination, mit der mittleres Auto erreicht wird
            l.append(h)
    if len(l) == 0:
        return False  # es gibt keine Kombinationsmöglichkeit zwischen 3,5 und 4
    else:
        return True  # es gibt Kombinationsmöglichkeiten zwischen 3,5 und 4


# Funktion um Fahrten auszusortieren, die nicht nah an 7,5 sind --> wenn ist_unter_7_5 True ist
def aussortieren_ueber_7_5(j, a):  # j ist betrachtete Zeile, a ist DataFrame mit Vergleichszeilen
    a = a.reset_index(drop=True)  # index neu setzen, da sonst Schleife nicht funktioniert
    t = 0
    Anzahl_Vergleichs_Zeilen = len(a.index)
    while t in range(Anzahl_Vergleichs_Zeilen):
        Gewicht = df.at[j, 'Gewicht_gesamt'] + a.at[t, 'Gewicht_gesamt']
        if Gewicht < 3.5 or Gewicht > 4:
            a = a.drop([t])
            t += 1
        else:
            t += 1
    return a


# Funktion zur Zuweisung Fahrzeug
def Fahrzeug(g):  # Zuordnung von Fahrzeug zu Gewicht, Eingabe ist Gewicht
    if g <= 1.2:  # Nutzlast
        type = 'lkw_3_5'
    elif g <= 4:
        type = 'lkw_7_5'
    else:
        type = 'lkw_18'
    return type


# Funktion zum Prüfen, ob zwei Aufträge gleiche Ziele haben, sagt nur ob es diese in DataFrame allg. gibt
def pruefe_gleiche_Ziele(j, a, b):  # i ist Auftrag, a ist DataFrame vom Auftrag, b ist VergleichsdataFrame
    b = b.reset_index(drop=True)
    Ziele_Auftrag_1 = [a.at[j, "Stopp_1"], a.at[j, "Stopp_2"], a.at[j, "Stopp_3"], a.at[j, "Ziel"]]
    Ergebnis_Ziele = []
    for t in range(len(b.index)):
        Ziele_Auftrag_2 = [b.at[t, "Stopp_1"], b.at[t, "Stopp_2"], b.at[t, "Stopp_3"], b.at[t, "Ziel"]]
        Antwort_einzelne_Zeile = []
        for m in range(len(Ziele_Auftrag_1)):
            if type(Ziele_Auftrag_1[m]) == str:  # nur Strings betrachtet --> nan ist float und wird übersprungen
                Wert = Ziele_Auftrag_1[m] in Ziele_Auftrag_2  # Prüfen, ob Wert in Liste enthalten
            else:
                Wert = False  # das sind die nans, die sind automatisch False
            if Wert is True:  # es gibt gleiche Ziele
                Antwort_einzelne_Zeile.append(1)
        if len(Antwort_einzelne_Zeile) > 0:
            Ergebnis_Ziele.append(1)
    if len(Ergebnis_Ziele) > 0:  # es gibt mind. 1 gleiches Ziel
        return True
    else:
        return False


# Funktion sortiert alle Zeilen aus, die andere Ziele haben Folge von pruefe_gleiche_Ziele == True
def aussortieren_andere_Ziele(j, a, b):  # i ist Auftrag, a ist DataFrame vom Auftrag, b ist VergleichsdataFrame
    b = b.reset_index(drop=True)
    Ziele_Auftrag_1 = [a.at[j, "Stopp_1"], a.at[j, "Stopp_2"], a.at[j, "Stopp_3"], a.at[j, "Ziel"]]
    for t in range(len(b.index)):
        Ziele_Auftrag_2 = [b.at[t, "Stopp_1"], b.at[t, "Stopp_2"], b.at[t, "Stopp_3"], b.at[t, "Ziel"]]
        Antwort_einzelne_Zeile = []
        for m in range(len(Ziele_Auftrag_1)):
            if type(Ziele_Auftrag_1[m]) == str:  # nan wird übersprungen, wäre nan in beiden Listen, wäre Antwort true
                Wert = Ziele_Auftrag_1[m] in Ziele_Auftrag_2
                if Wert == False:  # False=kein gleiches Ziel --> aussortieren
                    Antwort_einzelne_Zeile.append(1)
        if len(Antwort_einzelne_Zeile) > 0:
            b = b.drop([t])  # löschen aller Zeilen, die nicht gleiches Ziel haben
    b = b.reset_index(drop=True)
    return b


# Funktion bestimmt die kürzeste Reihenfolge der Stopps (über minimale Fahrzeit)
def reihenfolge_Stopps(j, t):  # i ist Zeile Auftrag in df, t ist Zeile Kombi in df also hier 0 und 1
    Liste_Stopps = [df.at[j, 'Ziel'], df.at[j, 'Stopp_1'], df.at[j, 'Stopp_2'], df.at[t, 'Ziel'], df.at[t, 'Stopp_1'],
                    df.at[t, 'Stopp_2']]
    Liste_Gewicht_f = [df.at[j, 'Gewicht Ziel'], df.at[j, 'Gewicht Stopp_1'], df.at[j, 'Gewicht Stopp_2'],
                     df.at[t, 'Gewicht Ziel'],
                     df.at[t, 'Gewicht Stopp_1'], df.at[t, 'Gewicht Stopp_2']]

    Start = 'GVZ'  # Ausgangsstartpunkt
    Reihenfolge = [np.nan] * 4  # Liste enthält Ziele in der Reihenfolge: [Stopp_1,Stopp_2,Stopp_3,Ziel]
    Reihenfolge_Gewicht = [np.nan] * 4
    h = 0

    # Restriktion JIT-Aufträge: JIT-Aufträge muss als erstes angefahren werden
    if pruefe_JIT(j, df) == True:
        if pruefe_Stopp_1(j) == True:  # z.B. wenn bereits vorher gebündelt
            Reihenfolge[3] = Liste_Stopps[1]
            Liste_Stopps[1] = np.nan  # aus der Liste löschen, da sonst doppelt zugeteilt
            Reihenfolge_Gewicht[3] = Liste_Gewicht_f[1]
            Liste_Gewicht_f[1] = np.nan
            Start = Reihenfolge[3]
        else:  # Auftrag hat noch keinen ersten Stopp, d.h. Ziel muss als erstes angefahren werden
            Reihenfolge[3] = Liste_Stopps[0]
            Liste_Stopps[0] = np.nan
            Reihenfolge_Gewicht[3] = Liste_Gewicht_f[0]
            Liste_Gewicht_f[0] = np.nan
            Start = Reihenfolge[3]

    while h >= 0:
        if pd.Series(Liste_Stopps).isnull().values.all() == True:
            # alle Ziele zugeordnet, d.h. alle Einträge in Liste_Stopps sind nan
            break  # Abbruchkriterium wenn alle Ziele in Reihenfolge
        Liste_Abstaende = []  # aus dieser Liste wird Minimum bestimmt
        for m in range(6):  # Ergebnis ist Liste mit den Abständen
            is_nan = pd.Series([Liste_Stopps[m]])
            if is_nan.isnull().values.any() == True:  # wenn Eintrag nan ist, nan hinzufügen
                Liste_Abstaende.append(np.nan)
            else:
                Liste_Abstaende.append(fahrzeit.at[Start, Liste_Stopps[m]])  # Abstand zu Startpunkt gesetzt
        Stelle_Min = Liste_Abstaende.index(np.nanmin(Liste_Abstaende))
        if type(Reihenfolge[3]) != str:  # noch kein Ziel zugewiesen, d.h. es steht nan dort
            Reihenfolge[3] = Liste_Stopps[Stelle_Min]  # Ziel zuweisen
            Reihenfolge_Gewicht[3] = Liste_Gewicht_f[Stelle_Min]  # Gewicht vom Ziel speichern
        else:
            if type(Reihenfolge[0]) != str:  # noch kein Stopp1 gesetzt
                Reihenfolge[0] = Reihenfolge[3]  # rutscht von der Position Ziel zur Position Stopp_1 auf
                Reihenfolge_Gewicht[0] = Reihenfolge_Gewicht[3]
                Reihenfolge[3] = Liste_Stopps[Stelle_Min]  # neues Ziel setzen
                Reihenfolge_Gewicht[3] = Liste_Gewicht_f[Stelle_Min]
            else:  # es gibt schon Stopp_1
                if type(Reihenfolge[1]) != str:  # noch kein Stopp_2 gesetzt
                    Reihenfolge[1] = Reihenfolge[3]  # aufrutschen des Ziels auf Position Stopp_2
                    Reihenfolge_Gewicht[1] = Reihenfolge_Gewicht[3]
                    Reihenfolge[3] = Liste_Stopps[Stelle_Min]
                    Reihenfolge_Gewicht[3] = Liste_Gewicht_f[Stelle_Min]
                else:  # es gibt schon Stopp_2
                    Reihenfolge[2] = Reihenfolge[3]  # hochrutschen des Ziels zu Position Stopp_3
                    Reihenfolge_Gewicht[2] = Reihenfolge_Gewicht[3]
                    Reihenfolge[3] = Liste_Stopps[Stelle_Min]  # neues Ziel setzen
                    Reihenfolge_Gewicht[3] = Liste_Gewicht_f[Stelle_Min]
        Liste_Stopps[Stelle_Min] = np.nan  # zugeordnet -> d.h. zu nan setzen
        Start = Reihenfolge[3]

        Ergebnis = Reihenfolge + Reihenfolge_Gewicht  # eine Liste aus beiden Listen erstellt
    return Ergebnis


# 1. Schritt: Zusammenfassen der JIT/JIS-Aufträge
# es gelten strengere Restriktionen beim Zusammenfassen, zwei JIT-Aufträge werden nicht zusammengefasst
# JIT-Ziel wird immer als erstes angefahren, nicht mit anderem JIT/JIS gebündelt

Zaehler_aus_df_entfernt_jit = []  # Zähler beschreibt Anzahl zusammengefasste Aufträge mit JIT
# bestimmen Fahrten mit hoher Fahrzeug-Auslastung
for i in range(len(df.index)):
    if df.at[i, "Gewicht_gesamt"] >= 1.1 and df.at[i, "Gewicht_gesamt"] <= 1.2:  # diese nicht bündeln, voll genug
        df.at[i, "nicht bündeln"] = 1
    elif df.at[i, "Gewicht_gesamt"] >= 3.8 and df.at[i, "Gewicht_gesamt"] <= 4:  # diese nicht bündeln, voll genug
        df.at[i, "nicht bündeln"] = 1
    else:
        df.at[i, "nicht bündeln"] = np.nan

i = 0
# Schleife zum Bündeln JIT-Aufträge
while i in range(len(df.index)):  # zwei zum testen sonst len(df.index)
    # Abbruchbedingung, falls alle zusammengefasst (Index + Länge von df verkürzt sich, wenn Aufträge zusammengefasst)
    if i > len(df.index) - 1 + len(Zaehler_aus_df_entfernt_jit) - 1:  # -1 jeweils, weil Länge nicht bei 0 beginnt
        break
    if pruefe_JIT(i, df) == True:
        if df.at[i, "nicht bündeln"] == 1:  # diese generell nicht bündeln wegen hoher Auslastung
            i += 1
        else:  # erzeugen Vergleichsdataframe --> welche kommen fürs Bündeln in Frage?
            df_Vergleich = erzeuge_vergleichs_df(i, Differenz_Abfahrten, 3600 / 2)  # Zeitfenster JIT?

            # Vergleichsdataframe aussortieren, Restriktionen werden nacheinander durchgegangen
            for m in range(len(df_Vergleich.index)):  # aussortieren JIT aus Vergleichsmatrix
                if pruefe_JIT(m, df_Vergleich) == True:  # nicht mit anderen JIT bündeln erlaubt
                    df_Vergleich = df_Vergleich.drop([m], axis=0)
            df_Vergleich = df_Vergleich.reset_index(drop=True)
            for m in range(len(df_Vergleich.index)):  # aussortieren JIT aus Vergleichsmatrix
                if df_Vergleich.at[m, "nicht bündeln"] == 1:  # nicht mit anderen JIT bündeln erlaubt
                    df_Vergleich = df_Vergleich.drop([m], axis=0)

            df_Vergleich = aussortieren_Gewicht(i, df_Vergleich)  # aussortieren nach zu großem Gewicht
            df_Vergleich = aussortieren_zu_viele_Stopps(i, df_Vergleich)  # aussortieren nach bereits zu vielen Stopps
            df_Vergleich = abstand_min(i, df_Vergleich)  # aussortieren nach zu weit weg
            df_Vergleich = aussortieren_nicht_buendeln(df_Vergleich)  # aussortieren wegen hoher Auslastung

            if df_Vergleich.empty == False:  # DataFrame noch nicht bereits leer
                # Beginn nach besten Kombinationen zu filtern: 1. nah bei 7,5 dann gleiches Ziel
                test_3_5 = ist_unter_3_5(df_Vergleich)  # prüfen, ob Fahrzeug unter 3,5 möglich
                if test_3_5 is True:
                    df_Vergleich = aussortieren_ueber_3_5(i, df_Vergleich)
                test_7_5 = ist_unter_7_5(df_Vergleich)  # prüfen, ob Fahrzeug unter 7,5 möglich
                if test_7_5 is True:  # möglich, diese bevorzugt bündeln
                    df_Vergleich = aussortieren_ueber_7_5(i, df_Vergleich)
                    df_Vergleich = df_Vergleich.reset_index(drop=True)
                if pruefe_gleiche_Ziele(i, df, df_Vergleich) == True:  # prüfen, ob es gleiche Ziele gibt (Weg)
                    df_Vergleich = aussortieren_andere_Ziele(i, df, df_Vergleich)
            if df_Vergleich.empty is True:  # Dataframe ist leer, d.h. keine Bündelung möglich
                i += 1
            else:  # Bündelung nach maximal erreichbaren Gewicht
                Liste_Gewicht = df_Vergleich["Gewicht_gesamt_neu"].tolist()
                index = Liste_Gewicht.index(
                    max(Liste_Gewicht))  # gibt Zeile in Dataframe wieder, mit der maximales Gewicht erreicht wird
                Zeile_Kombi = df_Vergleich.at[index, 'Index in df']  # Position in df (löschen aus df möglich)
                # print("Zeile " + str(i) + " sollte am besten kombinert werden mit Zeile " + str(Zeile_Kombi))
                Fahrzeugtyp = Fahrzeug(df.at[i, 'Gewicht_gesamt'] + df.at[Zeile_Kombi, 'Gewicht_gesamt'])
                Liste_Stopps_in_Reihenfolge = reihenfolge_Stopps(i, Zeile_Kombi)  # Bestimmen Stopp_1,...
                if Zeile_Kombi < i:  # um spätere Abfahrtszeit zu nehmen
                    Abfahrt = df.at[i, "Abfahrt"]
                    Ankunft = df.at[i, "Ankunft"]  # wegen JIT bei i
                else:
                    Abfahrt = df.at[Zeile_Kombi, "Abfahrt"]
                    Ankunft = df.at[i, "Ankunft"]  # wegen Kontrolle JIT erreicht, (wird als erstes angefahren)
                Gewicht_gesamt = df.at[i, 'Gewicht_gesamt'] + df.at[Zeile_Kombi, 'Gewicht_gesamt']
                if 1.1 <= Gewicht_gesamt <= 1.2:  # diese nicht bündeln, voll genug
                    buendel = 1
                elif 3.8 <= Gewicht_gesamt <= 4:  # diese nicht bündeln, voll genug
                    buendel = 1
                else:
                    buendel = np.nan
                # neuen Auftrag schreiben, wird am Ende angefügt
                df.loc[len(df.index) + 1, :] = [df.at[i, "Auftrags_ID"] + "_and_" + df.at[Zeile_Kombi, "Auftrags_ID"],
                                                df.at[i, "Start"], Liste_Stopps_in_Reihenfolge[0],
                                                Liste_Stopps_in_Reihenfolge[1], Liste_Stopps_in_Reihenfolge[2],
                                                Liste_Stopps_in_Reihenfolge[3], Abfahrt, Ankunft,
                                                Liste_Stopps_in_Reihenfolge[4], Liste_Stopps_in_Reihenfolge[5],
                                                Liste_Stopps_in_Reihenfolge[6], Liste_Stopps_in_Reihenfolge[7],
                                                Gewicht_gesamt, Fahrzeugtyp, df.at[i, "JIT/JIS"], buendel,
                                                df.at[i, "Produkt"]]
                df = df.drop([i, Zeile_Kombi])  # ursprüngliche 2 Zeilen löschen (da durch neue Zeile ersetzt)
                df = df.sort_values(by=['Abfahrt'])  # sortiert DataFrame nach Abfahrtszeit
                df = df.reset_index(drop=True)  # Index zurücksetzen
                Zaehler_aus_df_entfernt_jit.append(1)  # eins hoch zählen, da zusammengefasst
                if Zeile_Kombi < i:  # alles rutscht eins runter, darum noch einmal durchgehen, falls erneut gebündelt
                    i -= 1
    else:  # Auftrag ist nicht JIT, wird später betrachtet
        i += 1

# jit-Trips aus df löschen, in df_nur_jit gespeichert
df_nur_jit = df.copy()
for m in range(len(df_nur_jit.index)):  # Dataframe mit JIT-JIS-Aufträgen --> löschen andere
    if pruefe_JIT(m, df_nur_jit) == False:
        df_nur_jit = df_nur_jit.drop([m], axis=0)
for m in range(len(df.index)):  # Dataframe ohne JIT-JIS-Aufträgen --> löschen Jit/Jis-Aufträge
    if pruefe_JIT(m, df) == True:
        df = df.drop([m], axis=0)

df["nicht bündeln"] = [np.nan] * len(df.index)

# 2. Schritt: Zusammenfassen der restlichen Aufträge
Zaehler_aus_df_entfernt = []  # beschreibt, wie viele Aufträge zusammengefasst
df = df.reset_index(drop=True)
for i in range(len(df.index)):
    if df.at[i, "Gewicht_gesamt"] >= 1.1 and df.at[i, "Gewicht_gesamt"] <= 1.2:  # diese nicht bündeln, voll genug
        df.at[i, "nicht bündeln"] = 1
    elif df.at[i, "Gewicht_gesamt"] >= 3.8 and df.at[i, "Gewicht_gesamt"] <= 4:  # diese nicht bündeln, voll genug
        df.at[i, "nicht bündeln"] = 1
    else:
        df.at[i, "nicht bündeln"] = np.nan
i = 0
df = df.reset_index(drop=True)

# Erzeugen Vergleichsdataframe -> welche Aufträge kommen fürs Bündeln in Frage?
while i in range(len(df.index)):  # zwei zum testen sonst len(df.index)
    # Abbruchbedingung, falls alle zusammengefasst (Index + Länge von df verkürzt sich, wenn Aufträge zusammengefasst)
    if i > len(df.index) - 1 + len(Zaehler_aus_df_entfernt) - 1:  # -1 jeweils, weil Länge nicht bei 0 beginnt
        break
    if df.at[i, "nicht bündeln"] == 1:  # diese bereits voll ausgelastet
        i += 1
    else:  # erzeugen Vergleichsdataframe
        df_Vergleich = erzeuge_vergleichs_df(i, Differenz_Abfahrten, Differenz_Abfahrten)
        # Vergleichsdataframe aussortieren
        df_Vergleich = df_Vergleich.reset_index(drop=True)
        for m in range(len(df_Vergleich.index)):  # aussortieren JIT aus Vergleichsmatrix
            if df_Vergleich.at[m, "nicht bündeln"] == 1:
                df_Vergleich = df_Vergleich.drop([m], axis=0)
        df_Vergleich = aussortieren_Gewicht(i, df_Vergleich)  # aussortieren nach Gewicht
        df_Vergleich = aussortieren_zu_viele_Stopps(i, df_Vergleich)  # aussortieren nach zu vielen Stopps
        df_Vergleich = abstand_min(i, df_Vergleich)  # aussortieren zu großer Abstand
        df_Vergleich = aussortieren_nicht_buendeln(df_Vergleich)  # aussortieren wegen hoher Auslastung
        if df_Vergleich.empty == False:  # VergleichsDataFrame nicht leer
            # Beginn nach besten Kombinationen zu filtern: 1. nah bei 7,5, dann gleiches Ziel
            test_3_5 = ist_unter_3_5(df_Vergleich)  # prüfen, ob Fahrzeug unter 3,5 möglich ist
            if test_3_5 is True:
                df_Vergleich = aussortieren_ueber_3_5(i, df_Vergleich)
            test_7_5 = ist_unter_7_5(df_Vergleich)  # prüfen, ob Fahrzeug unter 7,5 möglich ist
            if test_7_5 is True:
                df_Vergleich = aussortieren_ueber_7_5(i, df_Vergleich)
            df_Vergleich = df_Vergleich.reset_index(drop=True)  # prüfen, ob es gleiche Ziele gibt (Weg)
            if pruefe_gleiche_Ziele(i, df, df_Vergleich) == True:
                df_Vergleich = aussortieren_andere_Ziele(i, df, df_Vergleich)

        if df_Vergleich.empty is True:  # Dataframe ist leer, d.h. keine Bündelung möglich
            i += 1
        else:  # Dataframe ist nicht leer --> beste Kombination suchen (max. Gewicht)
            Liste_Gewicht = df_Vergleich["Gewicht_gesamt_neu"].tolist()
            index = Liste_Gewicht.index(
                max(Liste_Gewicht))  # gibt Zeile in Dataframe wieder, mit der maximales Gewicht erreicht wird
            Zeile_Kombi = df_Vergleich.at[index, 'Index in df']
            # print("Zeile " + str(i) + " sollte am besten kombinert werden mit Zeile " + str(Zeile_Kombi))
            Fahrzeugtyp = Fahrzeug(df.at[i, 'Gewicht_gesamt'] + df.at[Zeile_Kombi, 'Gewicht_gesamt'])
            Liste_Stopps_in_Reihenfolge = reihenfolge_Stopps(i, Zeile_Kombi)  # bestimmen Stopp_1, Stopp_2, ...

            if Zeile_Kombi < i:
                Abfahrt = df.at[i, "Abfahrt"]
                Ankunft = df.at[i, "Ankunft"]
            else:
                Abfahrt = df.at[Zeile_Kombi, "Abfahrt"]
                Ankunft = df.at[i, "Ankunft"]  # eig egal?
            Gewicht_gesamt = df.at[i, 'Gewicht_gesamt'] + df.at[Zeile_Kombi, 'Gewicht_gesamt']
            if 1.1 <= Gewicht_gesamt <= 1.2:  # diese nicht bündeln, voll genug
                buendel = 1
            elif 3.8 <= Gewicht_gesamt <= 4:  # diese nicht bündeln, voll genug
                buendel = 1
            else:
                buendel = np.nan
            # neuen Auftrag schreiben
            df.loc[len(df.index) + 1, :] = [df.at[i, "Auftrags_ID"] + "_and_" + df.at[Zeile_Kombi, "Auftrags_ID"],
                                            df.at[i, "Start"], Liste_Stopps_in_Reihenfolge[0],
                                            Liste_Stopps_in_Reihenfolge[1], Liste_Stopps_in_Reihenfolge[2],
                                            Liste_Stopps_in_Reihenfolge[3], Abfahrt, Ankunft,
                                            Liste_Stopps_in_Reihenfolge[4], Liste_Stopps_in_Reihenfolge[5],
                                            Liste_Stopps_in_Reihenfolge[6], Liste_Stopps_in_Reihenfolge[7],
                                            Gewicht_gesamt, Fahrzeugtyp, df.at[i, "JIT/JIS"], buendel,
                                            df.at[i, "Produkt"]]

            df = df.drop([i, Zeile_Kombi])  # ursprüngliche 2 Zeilen löschen (da durch neue Zeile ersetzt)
            df = df.sort_values(by=['Abfahrt'])  # sortiert DataFrame nach Abfahrtszeit
            df = df.reset_index(drop=True)  # Index zurücksetzen
            Zaehler_aus_df_entfernt.append(1)  # eins hoch zählen, da zusammengefasst
            if Zeile_Kombi < i:  # alles rutscht eins runter, darum noch einmal durchgehen, falls erneut gebündelt
                i -= 1

gf = df.append(df_nur_jit)
gf = gf.sort_values(by=['Abfahrt'])  # sortiert DataFrame nach Abfahrtszeit
gf = gf.reset_index(drop=True)  # setzt Indizes zurück

# Zusammenfassen der Ergebnisse: ursprüngliche Anzahl Aufträge und neue Anzahl Aufträge
print("urspruengliche Anzahl Aufträge: " + str(Anzahl_Auftraege))
print("zusammengefasste Aufträge: " + str(len(Zaehler_aus_df_entfernt) + len(Zaehler_aus_df_entfernt_jit)))
print("neue Anzahl Aufträge: " + str(
    Anzahl_Auftraege - (len(Zaehler_aus_df_entfernt) + len(Zaehler_aus_df_entfernt_jit))))

# Schreiben alle gebündelten Aufträge/Touren als Excel-Tabelle
with pd.ExcelWriter('Tabellen_Excel\Fahrtenbuch_GVZ_gebuendelt_3_Stunden.xlsx') as writer:
    gf.to_excel(writer)